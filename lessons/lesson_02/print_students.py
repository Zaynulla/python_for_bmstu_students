# Формируем словарь, в котором ключ (key) - это номер группы, а значение (value) -
# список студентов группы
students = {
    "АК1-111": [
        "Иванов Иван Иваныч",
        "Кузьмин Кузьма Кузьмич",
        "Петров Пётр Петрович",
    ],
    "АК2-111": [
        "Иванов Иван Иваныч",
        "Кузьмин Кузьма Кузьмич",
        "Петров Пётр Петрович",
    ],
    "АК2-112": [],
    "СМ2-111": [
        # Фамилия Имя Отчество в алфавитном порядке
        "Иванов Иван Иваныч",
        "Кузьмин Кузьма Кузьмич",
        "Петров Пётр Петрович",
    ],
}

# Условие ниже можно убрать, необходимо для того, чтобы этот блок кода не выполнялся,
# когда запускаем юнит-тесты. Лекция про юнит-тесты также представлена в курсе.
if __name__ == "__main__":
    # for key, value in dict.items()
    for group_name, group_students in students.items():
        print(group_name)
        for student_name in group_students:
            print(student_name)
