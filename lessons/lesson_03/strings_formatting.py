# %%
name = "Иван Иванов"
# %%
# Старый способ форматировать строк
print("Привет, %s!" % name)

# %%
# Более удобный метода format или f-строки
print(f"Привет, {name}!")

# %%
# В Python есть 2 вида кавычек
print("Многие слышали о Жанне д'Арк")
print('Апостроф - от др. греческого "обращенный назад"')


# %%
# И есть "многострочные" строки
some_text = """А этот текст очень длинный, он не умещается в одной строке.
    Строка может быть очень длинной, например, это может быть SQL запрос к базе данных.
    """
print(some_text)

# %%
some_text_formatted = f"""А самое интересное, что и внутри таких "многострочных" строк
можно использовать переменные и даже одинарные кавычки, в т.ч. того же типа. Можно также
использовать переменные, если перед строкой поставить символ 'f', т.е. превратить её в
'f-строку'. Т.е. для такой длинной строки работают все методы, что и для обычной строки.
Например, name по прежнему содержит строку '{name}', a 5+2={5+2}. И таких полей в строке
может быть много.
"""

# Перед выводом на печать заменим символы переноса строки на пробел и приведём все буквы
# к верхнему регистру
print(some_text_formatted.replace("\n", " ").upper())

# %%
# А ещё строки можно умножать
print("Hi! " * 5)
