import matplotlib.pyplot as plt
import numpy as np

# Задаем параметры швеллера
h = 2.5  # мм- Толщина уголка
B = 40  # мм- Длина стороны уголка
Delt = 1.6  # мм- Толщина присоединенной обшивки
L = 54  # мм- Длина присоединенной обшивки
Ynl = 9  # мм- расстояние от нижней кромки обшивки до нейтральной линии

# Размер картинки
LfigX = 7  # дюйм- Размер картинки по Х
LfigY = 5  # дюйм- Размер картинки по Y
Zaz = 10  # мм- зазор между обшивкой и краем картинки
LfigXmm = LfigX * 25.4  # мм- Размер картинки

# Размерные линии
ii = 4  # мм - расстояние от линии до размера
hh = 2  # мм - высота шрифта


z = (B + Delt) / 2

# Создаем массив точек для построения профиля
# Профиль уголка
x1 = np.array([-B / 2, -B / 2, B / 2, B / 2, -B / 2 + h, -B / 2 + h, -B / 2])
y1 = np.array([z, -z + Delt, -z + Delt, -z + Delt + h, -z + Delt + h, z, z])
# Профиль обшивки
x2 = np.array([-L / 2, -L / 2, L / 2, L / 2, -L / 2])
y2 = np.array([-z + Delt, -z, -z, -z + Delt, -z + Delt])

# Уравнение нейтральной линии
x3 = np.array([-L / 2, L / 2])
y3 = np.array([-z + Ynl, -z + Ynl])

# Отрисовка размерных  линий
# Размер h
x4 = np.array([-B / 2, -B / 2])
y4 = np.array([z, z + ii])
x5 = np.array([-B / 2 + h, -B / 2 + h])
y5 = np.array([z, z + ii])
x6 = np.array([-B / 2 - ii / 2, -B / 2 + h + ii])
y6 = np.array([z + ii, z + ii])
# Размер B
x7 = np.array([-B / 2 - ii, -B / 2 - ii])
y7 = np.array([-z + Delt, z])
x8 = np.array([-B / 2 - ii, -B / 2])
y8 = np.array([z, z])
# Размер L
x9 = np.array([-L / 2, -L / 2])
y9 = np.array([-z, -z - ii])
x10 = np.array([L / 2, L / 2])
y10 = np.array([-z, -z - ii])
x11 = np.array([-L / 2, L / 2])
y11 = np.array([-z - ii, -z - ii])
# Размер Delta
x12 = np.array([L / 2, L / 2 + ii])
y12 = np.array([-z, -z])
x13 = np.array([L / 2, L / 2 + ii])
y13 = np.array([-z + Delt, -z + Delt])
x14 = np.array([L / 2 + ii, L / 2 + ii])
y14 = np.array([-z, -z + ii + hh])
# Размер Nl
x15 = np.array([-L / 2, -L / 2 - ii])
y15 = np.array([-z, -z])
x16 = np.array([-L / 2, -L / 2 - ii])
y16 = np.array([-z + Ynl, -z + Ynl])
x17 = np.array([-L / 2 - ii, -L / 2 - ii])
y17 = np.array([-z, -z + Ynl])


def plot_all_strng():
    plt.figure(figsize=(LfigX, LfigY))  # Создать картинку с размерами Lfig
    plt.axes(
        xlim=(-(L / 2 + Zaz), (L / 2 + Zaz)),
        ylim=(-(L / 2 + Zaz) * LfigY / LfigX, ((L / 2 + Zaz) * LfigY / LfigX)),
    )  # Масштабы картинки по осям
    plt.plot(x1, y1, "k", lw=1.5)  # Отрисовка первого графика. Толщина 1.5
    plt.plot(x2, y2, "k", lw=1.5)  # Отрисовка Второго графика
    # Отрисовка нейтральной линии. -. обозначает штрих пунктир
    plt.plot(x3, y3, "k-.", lw=1)

    # Размерные линии
    # Размер h
    plt.plot(x4, y4, "k", lw=1)
    plt.plot(x5, y5, "k", lw=1)
    plt.plot(x6, y6, "k", lw=1)
    # Размер B
    plt.plot(x7, y7, "k", lw=1)
    plt.plot(x8, y8, "k", lw=1)
    # Размер L
    plt.plot(x9, y9, "k", lw=1)
    plt.plot(x10, y10, "k", lw=1)
    plt.plot(x11, y11, "k", lw=1)
    # Размер Delta
    plt.plot(x12, y12, "k", lw=1)
    plt.plot(x13, y13, "k", lw=1)
    plt.plot(x14, y14, "k", lw=1)
    # Размер Nl
    plt.plot(x15, y15, "k", lw=1)
    plt.plot(x16, y16, "k", lw=1)
    plt.plot(x17, y17, "k", lw=1)

    # Подпись размеров
    plt.text(-B / 2 + h + 1, z + ii + 0.5, h)  # Размер h
    plt.text(-B / 2 - ii - 4, 0, B)  # Размер B
    plt.text(0, -z - ii + 0.5, L)  # Размер L
    plt.text(L / 2, -z + ii + 0.5, Delt)  # Размер Delt
    plt.text(-L / 2 - ii - hh * 2, -z + Ynl / 2, Ynl)  # Размер Ynl

    plt.fill_between(x1, y1, facecolor="none", hatch="/", linewidth=0.0)
    plt.fill_between(x2, y2, facecolor="none", hatch="X", linewidth=0.0)

    plt.axis("off")  # Отключить отображение осей
    # plt.show()  # Отрисовка картинки
    plt.savefig("dz/konstruirovanie_korpusov_la_9sem/Ugolok.png")
