import matplotlib.pyplot as plt
import numpy as np
from docx.shared import Cm
from docxtpl import DocxTemplate, InlineImage

# Исходные данные

E = 72e9  # Модуль упругости, [Па]
delta = 0.0012  # Толщина обшивки, [м]
D = 3  # Диаметр отсека, [м]
R = D / 2  # Радиус отсека, [м]
n = 160  # Количество стрингеров
eps_lim = 0.02  # Конечная относительная деформация отсека
stress_strength = 280e6  # Предел упругости, [Па]
F_str = 236e-6  # Площадь стрингера, [м ^ 2]
F_shell = 71e-6  # Площадь кусочка обшивки, [м ^ 2]

# Расчётная часть

t_s = (np.pi * D) / n  # Шаг стрингеров, [м]
stress_krit = (
    0.2 * E * (delta / D)
    + 3.6 * E * (delta / t_s) ** 2  # Критическое напряжение для отсека, [Па]
)


def stress_shell(eps: float) -> float:
    """Кусочная функция, возвращающая напряжение в обшивке в зависимости от
    относительной деформации.

    Parameters
    ----------
    eps : float
        Относительная деформация

    Returns
    -------
    float
        Напряжение
    """
    if eps <= -stress_krit / E:
        return -stress_krit
    if -stress_krit / E < eps < stress_strength / E:
        return E * eps
    if eps >= stress_strength / E:
        return stress_krit


def stress_str(eps: float) -> float:
    """Кусочная функция, возвращающая напряжение в стрингере в зависимости от
    относительной деформации.

    Parameters
    ----------
    eps : float
        Относительная деформация

    Returns
    -------
    float
        Напряжение
    """
    if eps <= -stress_strength / E:
        return -stress_strength
    if -eps_lim < eps < stress_strength / E:
        return E * eps
    if eps >= stress_strength / E:
        return stress_strength


N = np.array([])
M = np.array([])
# Цикл, рассчитывающий каждое новое положение сечения при сжимающих
# и изгибающих усилиях
eps_down = -eps_lim
for i in range(50):
    N_str = 0  # Суммарная осевая нагрузка в стрингерах
    N_shell = 0  # Суммарная осевая нагрузка в обшивке
    N_summ = 0  # Суммарная осевая нагрузка
    M_str = 0  # Суммарный момент в стрингерах
    M_shell = 0  # Суммарный момент в обшивке
    M_summ = 0  # Суммарный момент
    eps_up = -eps_lim + 2 * eps_lim / 50 * i
    # Цикл для расчёта нагрузки  для i-го положения сечения
    for j in range(160):
        y_str_j = np.cos(2 * np.pi * j / 160) * R  # Координата j-го стрингера
        y_shell_j = (
            np.cos(2 * np.pi / 320 + 2 * np.pi * j / 160) * R
        )  # Координата j-ой части обшивки
        eps_str_j = (y_str_j + R) / D * (eps_up - eps_down) + eps_down
        eps_shell_j = (y_shell_j + R) / D * (eps_up - eps_down) + eps_down
        N_str = N_str + stress_str(eps_str_j) * F_str
        N_shell = N_shell + stress_shell(eps_shell_j) * F_shell
        M_str = M_str + (stress_str(eps_str_j) * F_str) * y_str_j
        M_shell = M_shell + (stress_shell(eps_shell_j) * F_shell) * y_shell_j
    N_summ = N_str + N_shell
    M_summ = M_str + M_shell
    N = np.append(N, N_summ)
    M = np.append(M, M_summ)

# Цикл, рассчитывающий каждое новое положение сечения при растягивающих и изгибающих
# усилиях
eps_up = eps_lim
for i in range(50):
    N_str = 0  # Суммарная осевая нагрузка в стрингерах
    N_shell = 0  # Суммарная осевая нагрузка в обшивке
    N_summ = 0  # Суммарная осевая нагрузка
    M_str = 0  # Суммарный момент в стрингерах
    M_shell = 0  # Суммарный момент в обшивке
    M_summ = 0  # Суммарный момент
    eps_down = -eps_lim + 2 * eps_lim / 50 * i
    # Цикл для расчёта нагрузки  для i-го положения сечения
    for j in range(160):
        y_str_j = np.cos(2 * np.pi * j / 160) * R  # Координата j-го стрингера
        y_shell_j = (
            np.cos(2 * np.pi / 320 + 2 * np.pi * j / 160) * R
        )  # Координата j-ой части обшивки
        eps_str_j = (y_str_j + R) / D * (eps_up - eps_down) + eps_down
        eps_shell_j = (y_shell_j + R) / D * (eps_up - eps_down) + eps_down
        N_str = N_str + stress_str(eps_str_j) * F_str
        N_shell = N_shell + stress_shell(eps_shell_j) * F_shell
        M_str = M_str + (stress_str(eps_str_j) * F_str) * y_str_j
        M_shell = M_shell + (stress_shell(eps_shell_j) * F_shell) * y_shell_j
    N_summ = N_str + N_shell
    M_summ = M_str + M_shell
    N = np.append(N, N_summ)
    M = np.append(M, M_summ)

# Цикл, рассчитывающий каждое новое положение сечения при растягивающих и
# изгибающих усилиях
eps_down = eps_lim
for i in range(50):
    N_str = 0  # Суммарная осевая нагрузка в стрингерах
    N_shell = 0  # Суммарная осевая нагрузка в обшивке
    N_summ = 0  # Суммарная осевая нагрузка
    M_str = 0  # Суммарный момент в стрингерах
    M_shell = 0  # Суммарный момент в обшивке
    M_summ = 0  # Суммарный момент
    eps_up = eps_lim - 2 * eps_lim / 50 * i
    # Цикл для расчёта нагрузки  для i-го положения сечения
    for j in range(160):
        y_str_j = np.cos(2 * np.pi * j / 160) * R  # Координата j-го стрингера
        y_shell_j = (
            np.cos(2 * np.pi / 320 + 2 * np.pi * j / 160) * R
        )  # Координата j-ой части обшивки
        eps_str_j = (y_str_j + R) / D * (eps_up - eps_down) + eps_down
        eps_shell_j = (y_shell_j + R) / D * (eps_up - eps_down) + eps_down
        N_str = N_str + stress_str(eps_str_j) * F_str
        N_shell = N_shell + stress_shell(eps_shell_j) * F_shell
        M_str = M_str + (stress_str(eps_str_j) * F_str) * y_str_j
        M_shell = M_shell + (stress_shell(eps_shell_j) * F_shell) * y_shell_j
    N_summ = N_str + N_shell
    M_summ = M_str + M_shell
    N = np.append(N, N_summ)
    M = np.append(M, M_summ)

# Цикл, рассчитывающий каждое новое положение сечения при сжимающих и
# изгибающих усилиях
eps_up = -eps_lim
for i in range(50):
    N_str = 0  # Суммарная осевая нагрузка в стрингерах
    N_shell = 0  # Суммарная осевая нагрузка в обшивке
    N_summ = 0  # Суммарная осевая нагрузка
    M_str = 0  # Суммарный момент в стрингерах
    M_shell = 0  # Суммарный момент в обшивке
    M_summ = 0  # Суммарный момент
    eps_down = eps_lim - 2 * eps_lim / 50 * i
    # Цикл для расчёта нагрузки  для i-го положения сечения
    for j in range(160):
        y_str_j = np.cos(2 * np.pi * j / 160) * R  # Координата j-го стрингера
        y_shell_j = (
            np.cos(2 * np.pi / 320 + 2 * np.pi * j / 160) * R
        )  # Координата j-ой части обшивки
        eps_str_j = (y_str_j + R) / D * (eps_up - eps_down) + eps_down
        eps_shell_j = (y_shell_j + R) / D * (eps_up - eps_down) + eps_down
        N_str = N_str + stress_str(eps_str_j) * F_str
        N_shell = N_shell + stress_shell(eps_shell_j) * F_shell
        M_str = M_str + (stress_str(eps_str_j) * F_str) * y_str_j
        M_shell = M_shell + (stress_shell(eps_shell_j) * F_shell) * y_shell_j
    N_summ = N_str + N_shell
    M_summ = M_str + M_shell
    N = np.append(N, N_summ)
    M = np.append(M, M_summ)


# Построение диаграммы Прандтля для стрингеров

fig = plt.figure()
plt.plot(
    [-0.01, -stress_strength / E, stress_strength / E, 0.01],
    [-stress_strength, -stress_strength, stress_strength, stress_strength],
)
plt.grid()
plt.title("Диаграмма Прандтля для стрингера")
plt.xlabel("eps")
plt.ylabel("sigma, Па")
plt.show()
fig.savefig("fig2")

# Построение диаграммы Прандтля для обшивки

fig = plt.figure()
plt.plot(
    [-0.01, -stress_krit / E, stress_strength / E, 0.01],
    [-stress_krit, -stress_krit, stress_strength, stress_strength],
)
plt.grid()
plt.title("Диаграмма Прандтля для обшивки")
plt.xlabel("eps")
plt.ylabel("sigma, Па")
plt.show()
fig.savefig("fig3")

# Построение диаграммы внешних нагрузок

fig = plt.figure()
plt.plot(M, N)
plt.grid()
plt.title("Диаграмма сил и моментов")
plt.xlabel("M, Нм")
plt.ylabel("N, Н")
plt.show()
fig.savefig("fig4")

# Создание документа

image_prandtl_stringer_path = "fig2.png"
image_prandtl_shell_path = "fig3.png"
image_diagramma_path = "fig4.png"

docx_template = DocxTemplate("doc/auto_generated_doc_template.docx")

context = {
    "image2": InlineImage(docx_template, image_prandtl_stringer_path, Cm(11)),
    "image3": InlineImage(docx_template, image_prandtl_shell_path, Cm(11)),
    "image4": InlineImage(docx_template, image_diagramma_path, Cm(15)),
    "E": E,
    "D": D,
    "delta": delta,
    "F_str": F_str,
    "F_shell": F_shell,
    "eps_lim": eps_lim,
    "stress_strength": stress_strength,
    "n": n,
    "stress_krit": stress_krit,
}

docx_template.render(context)
docx_template.save("doc/auto_generated_report.docx")
