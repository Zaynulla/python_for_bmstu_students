import datetime

from docx.shared import Cm
from docxtpl import DocxTemplate, InlineImage

from utils.desicion_parameters import catch_mass as mass
from utils.desicion_parameters import elastic_modulus as em
from utils.desicion_parameters import force_tension_start as T0
from utils.desicion_parameters import length_loaded as length
from utils.desicion_parameters import section_area_wire as fi


def convert_to_docx():
    doc = DocxTemplate("./dz/non_linear_1d_vibration/utils/auto_docx.docx")
    coord_from_time = InlineImage(
        doc, "./dz/non_linear_1d_vibration/utils/w_m.png", width=Cm(12)
    )
    animation = InlineImage(
        doc, "./dz/non_linear_1d_vibration/utils/animation.gif", width=Cm(12)
    )
    phase_portrait = InlineImage(
        doc, "./dz/non_linear_1d_vibration/utils/phase_portrait.gif", width=Cm(12)
    )

    context = {
        "students": "Агузаров А.Д., Вахрушин Я.А., Карташев И.М., Пелин А.А.",
        "master": "Жумаев З.С.",
        "title": "Динамический гаситель с одной степенью свободы",
        "year": datetime.datetime.now().year,
        "libraries": "docx, docxtpl, matplotlib, numpy и sympy.",
        "mass": mass,
        "em": em / 10**9,
        "T0": round(T0, 2),
        "length": length,
        "fi": round(fi * 10**6, 2),
        "image1": coord_from_time,
        "image2": animation,
        "image3": phase_portrait,
    }

    # Render automated report
    doc.render(context)
    doc.save("./dz/non_linear_1d_vibration/utils/report.docx")
